import QtQuick

Rectangle {
    width: buttonTextId.implicitWidth + 20
    height: buttonTextId.implicitHeight + 20
    color: "red"
    border { color: "blue"; width: 3 }

    Text {
        id: buttonTextId
        text: "Button"
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked:  {
            console.log("Clicked On: " + buttonTextId.text)
        }
    }
}
